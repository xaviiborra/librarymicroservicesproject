package com.certiorem.librarymicroservicesproject.application;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class ConfigurationServiceApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(ConfigurationServiceApplication.class).run(args);
	}

}
